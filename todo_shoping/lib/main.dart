import 'dart:async';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'models/todo.dart';
import 'models/todo_manager.dart';
import 'widgets/create_todo.dart';
import 'widgets/edit_todo.dart';
import 'widgets/todo_status_widget.dart';

final Map<String, Item> _items = <String, Item>{};
Item _itemForMessage(Map<String, dynamic> message) {
  final dynamic data = message['data'] ?? message;
  final String itemId = data['id'];
  final Item item = _items.putIfAbsent(itemId, () => Item(itemId: itemId))
    ..status = data['status'];
  return item;
}

class Item {
  Item({this.itemId});
  final String itemId;

  StreamController<Item> _controller = StreamController<Item>.broadcast();
  Stream<Item> get onChanged => _controller.stream;

  String _status;
  String get status => _status;
  set status(String value) {
    _status = value;
    _controller.add(this);
  }

  static final Map<String, Route<void>> routes = <String, Route<void>>{};
  Route<void> get route {
    final String routeName = '/detail/$itemId';
    return routes.putIfAbsent(
      routeName,
      () => MaterialPageRoute<void>(
        settings: RouteSettings(name: routeName),
        builder: (BuildContext context) => DetailPage(itemId),
      ),
    );
  }
}

class DetailPage extends StatefulWidget {
  DetailPage(this.itemId);
  final String itemId;
  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  Item _item;
  StreamSubscription<Item> _subscription;

  @override
  void initState() {
    super.initState();
    _item = _items[widget.itemId];
    _subscription = _item.onChanged.listen((Item item) {
      if (!mounted) {
        _subscription.cancel();
      } else {
        setState(() {
          _item = item;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Item ${_item.itemId}"),
      ),
      body: Material(
        child: Center(child: Text("Item status: ${_item.status}")),
      ),
    );
  }
}

void main() {
  // Set `enableInDevMode` to true to see reports while in debug mode
  // This is only to be used for confirming that reports are being
  // submitted as expected. It is not intended to be used for everyday
  // development.
  Crashlytics.instance.enableInDevMode = true;

  // Pass all uncaught errors to Crashlytics.
  FlutterError.onError = Crashlytics.instance.recordFlutterError;

  runZoned(() {
    runApp(MyApp());
  }, onError: Crashlytics.instance.recordError);
}

class MyApp extends StatelessWidget {
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: analytics);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Todo Shoping',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      navigatorObservers: <NavigatorObserver>[observer],
      home: TodoHomePage(
        analytics: analytics,
        observer: observer,
      ),
      routes: <String, WidgetBuilder>{
        '/create_todo': (BuildContext context) => CreateTodo(),
        '/edit_todo': (BuildContext context) => EditTodo(),
      },
    );
  }
}

class TodoHomePage extends StatefulWidget {
  static final dbHelper = TodoManager.instance;
  final FirebaseAnalytics analytics;
  final FirebaseAnalyticsObserver observer;

  TodoHomePage({Key key, this.analytics, this.observer}) : super(key: key);

  @override
  _TodoHomePageState createState() => _TodoHomePageState(analytics, observer);
}

class _TodoHomePageState extends State<TodoHomePage> {
  final FirebaseAnalyticsObserver observer;
  final FirebaseAnalytics analytics;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  _TodoHomePageState(this.analytics, this.observer);

  Widget _buildDialog(BuildContext context, Item item) {
    return AlertDialog(
      content: Text("Item ${item.itemId} has been updated"),
      actions: <Widget>[
        FlatButton(
          child: const Text('CLOSE'),
          onPressed: () {
            Navigator.pop(context, false);
          },
        ),
        FlatButton(
          child: const Text('SHOW'),
          onPressed: () {
            Navigator.pop(context, true);
          },
        ),
      ],
    );
  }

  void _showItemDialog(Map<String, dynamic> message) {
    showDialog<bool>(
      context: context,
      builder: (_) => _buildDialog(context, _itemForMessage(message)),
    ).then((bool shouldNavigate) {
      if (shouldNavigate == true) {
        _navigateToItemDetail(message);
      }
    });
  }

  void _navigateToItemDetail(Map<String, dynamic> message) {
    final Item item = _itemForMessage(message);
    // Clear away dialogs
    Navigator.popUntil(context, (Route<dynamic> route) => route is PageRoute);
    if (!item.route.isCurrent) {
      Navigator.push(context, item.route);
    }
  }

  final _formKey = GlobalKey<FormState>();

  final searchController = TextEditingController();

  String searchText = '';

  Future<void> _sendAnalyticsEvent() async {
    await analytics.logEvent(
      name: 'Todo_Create_Request',
      parameters: <String, dynamic>{
        'string': 'Todo Create',
      },
    );
  }

  void _delete(int id, BuildContext context) async {
    final rowsDeleted = await TodoHomePage.dbHelper.delete(id);
    print('deleted $rowsDeleted row(s): row $id');
    Navigator.of(context).pop();
    Navigator.pushReplacementNamed(context, '/');
  }

  Future<List<Map<String, dynamic>>> _searchData(String searchStr) async {
    return TodoHomePage.dbHelper.queryAllRows(searchStr);
  }

  void _setSearchText() {
    setState(() {
      searchText = searchController.text;
    });
  }

  void initState() {
    searchController.addListener(_setSearchText);
    super.initState();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        _showItemDialog(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        _navigateToItemDetail(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        _navigateToItemDetail(message);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Todo Shoping"), actions: <Widget>[
        // action button
        IconButton(
          icon: Icon(Icons.home),
          onPressed: () {
            Navigator.pushReplacementNamed(context, '/');
          },
        ),
      ]),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.1,
                width: MediaQuery.of(context).size.width,
                child: Form(
                  key: _formKey,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        height: MediaQuery.of(context).size.height * 0.1,
                        child: TextFormField(
                          controller: searchController,
                        ),
                      ),
                      Container(
                          height: MediaQuery.of(context).size.height * 0.07,
                          width: MediaQuery.of(context).size.width * 0.1,
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Theme.of(context).primaryColor,
                                  width: 2.0)),
                          child: Icon(Icons.search))
                    ],
                  ),
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.7,
                width: MediaQuery.of(context).size.width,
                child: FutureBuilder<List<Map<String, dynamic>>>(
                    future: _searchData(searchText),
                    builder: (context,
                        AsyncSnapshot<List<Map<String, dynamic>>> todoList) {
                      if (todoList.hasData) {
                        return ListView.builder(
                          itemCount: todoList.data.length,
                          itemBuilder: (BuildContext ctx, int index) {
                            return Container(
                              decoration: BoxDecoration(
                                border: Border(
                                    bottom: new BorderSide(color: Colors.grey)),
                              ),
                              child: ListTile(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 10.0),
                                leading: TodoStatusWidget(
                                    todoStatus: TodoStatus.values[int.parse(
                                        todoList.data[index]['todoStatus']
                                            .toString())]),
                                title: Text(todoList.data[index]['title']),
                                subtitle: Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Text(
                                          '${todoList.data[index]['quantity']}  ${todoList.data[index]['unit']}',
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        TodoStatus.values[int.parse(todoList
                                                        .data[index]
                                                            ['todoStatus']
                                                        .toString())]
                                                    .toString()
                                                    .split('.')
                                                    .last ==
                                                'completed'
                                            ? Text(
                                                '${todoList.data[index]['total']}\$',
                                              )
                                            : Text(
                                                '${todoList.data[index]['budget']}\$',
                                              ),
                                        Container(
                                            margin: EdgeInsets.symmetric(
                                                horizontal: 10),
                                            child: Text(TodoType.values[
                                                    int.parse(todoList
                                                        .data[index]['todoType']
                                                        .toString())]
                                                .toString()
                                                .split('.')
                                                .last))
                                      ],
                                    ),
                                  ],
                                ),
                                trailing: Container(
                                  width: MediaQuery.of(context).size.width / 4,
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: IconButton(
                                          icon: Icon(Icons.edit),
                                          onPressed: () {
                                            Navigator.pushReplacementNamed(
                                                context, '/edit_todo',
                                                arguments: <String, dynamic>{
                                                  "id": todoList.data[index]
                                                      ['id'],
                                                  "title": todoList.data[index]
                                                      ['title'],
                                                  "todoType": todoList
                                                      .data[index]['todoType'],
                                                  "maxPrice": todoList
                                                      .data[index]['maxPrice'],
                                                  "quantity": todoList
                                                      .data[index]['quantity'],
                                                  "unit": todoList.data[index]
                                                      ['unit'],
                                                  "budget": todoList.data[index]
                                                      ['budget'],
                                                  "price": todoList.data[index]
                                                      ['price'],
                                                  "total": todoList.data[index]
                                                      ['total'],
                                                  "remarks": todoList
                                                      .data[index]['remarks'],
                                                  "entryDate": todoList
                                                      .data[index]['entryDate'],
                                                  "todoStatus":
                                                      todoList.data[index]
                                                          ['todoStatus'],
                                                  "completionDate":
                                                      todoList.data[index]
                                                          ['completionDate'],
                                                  "completionNote":
                                                      todoList.data[index]
                                                          ['completionNote'],
                                                });
                                          },
                                        ),
                                      ),
                                      Expanded(
                                        child: IconButton(
                                            icon: Icon(Icons.delete),
                                            onPressed: () {
                                              showDialog(
                                                  context: context,
                                                  builder:
                                                      (context) => AlertDialog(
                                                            title: Text(
                                                                'Are you sure?'),
                                                            content: Text(
                                                                'Do you want to remove ${todoList.data[index]['title']}?'),
                                                            actions: <Widget>[
                                                              FlatButton(
                                                                  onPressed:
                                                                      () {
                                                                    _delete(
                                                                        int.parse(todoList
                                                                            .data[index]['id']
                                                                            .toString()),
                                                                        context);
                                                                  },
                                                                  child: Text(
                                                                      'Yes')),
                                                              FlatButton(
                                                                  onPressed: () =>
                                                                      Navigator.of(
                                                                              context)
                                                                          .pop(),
                                                                  child: Text(
                                                                      'No'))
                                                            ],
                                                          ));
                                            }),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        );
                      } else {
                        return CircularProgressIndicator();
                      }
                    }),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          //Crashlytics.instance.crash();
          _sendAnalyticsEvent();
          Navigator.pushReplacementNamed(context, '/create_todo');
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
