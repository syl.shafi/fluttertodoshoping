import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../models/todo.dart';
import '../models/todo_manager.dart';

class EditTodo extends StatefulWidget {
  final dbHelper = TodoManager.instance;

  @override
  EditTodoState createState() {
    return new EditTodoState();
  }
}

class EditTodoState extends State<EditTodo> {
  final _formKey = GlobalKey<FormState>();

  final titleController = TextEditingController();
  final maxPriceController = TextEditingController();
  final quantityController = TextEditingController();
  final unitController = TextEditingController();
  final budgetController = TextEditingController();
  final priceController = TextEditingController();
  final totalController = TextEditingController();
  final remarksController = TextEditingController();
  final completionNoteController = TextEditingController();

  bool pageShifted = true;

  String id;
  String title;
  TodoType todoType = TodoType.kitchen;
  double maxPrice = 0;
  double quantity = 0;
  String unit;
  double budget = 0;
  double price = 0;
  double total = 0;
  String remarks;
  DateTime entryDate = DateTime.now();
  TodoStatus todoStatus = TodoStatus.listed;
  DateTime completionDate = DateTime.now();
  String completionNote;

  bool triggerController = true;

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  calculateMaxPrice() {
    if (triggerController) {
      double maxPriceTemp = 0.0;

      if (quantityController.text.isEmpty || budgetController.text.isEmpty) {
        maxPriceTemp = 0.0;
      } else if (!isNumeric(quantityController.text) ||
          !isNumeric(budgetController.text)) {
        maxPriceTemp = 0.0;
      } else if (double.parse(quantityController.text) == 0) {
        maxPriceTemp = 0.0;
      } else {
        maxPriceTemp = (double.parse(budgetController.text) /
            double.parse(quantityController.text));
      }
      triggerController = false;
      maxPriceController.text = maxPriceTemp.toString();
      maxPrice = maxPriceTemp;
      triggerController = true;
    }
  }

  calculateBudget() {
    if (triggerController) {
      double budgetTemp = 0.0;

      if (quantityController.text.isEmpty || maxPriceController.text.isEmpty) {
        budgetTemp = 0.0;
      } else if (!isNumeric(quantityController.text) ||
          !isNumeric(maxPriceController.text)) {
        budgetTemp = 0.0;
      } else {
        budgetTemp = (double.parse(maxPriceController.text) *
            double.parse(quantityController.text));
      }
      triggerController = false;
      budgetController.text = budgetTemp.toString();
      budget = budgetTemp;
      triggerController = true;
    }
  }

  calculatePrice() {
    if (triggerController) {
      double priceTemp = 0.0;

      if (quantityController.text.isEmpty || totalController.text.isEmpty) {
        priceTemp = 0.0;
      } else if (!isNumeric(quantityController.text) ||
          !isNumeric(totalController.text)) {
        priceTemp = 0.0;
      } else if (double.parse(quantityController.text) == 0) {
        priceTemp = 0.0;
      } else {
        priceTemp = (double.parse(totalController.text) /
            double.parse(quantityController.text));
      }
      triggerController = false;
      priceController.text = priceTemp.toString();
      price = priceTemp;
      triggerController = true;
    }
  }

  calculateTotal() {
    if (triggerController) {
      double totalTemp = 0.0;

      if (quantityController.text.isEmpty || priceController.text.isEmpty) {
        totalTemp = 0.0;
      } else if (!isNumeric(quantityController.text) ||
          !isNumeric(priceController.text)) {
        totalTemp = 0.0;
      } else {
        totalTemp = (double.parse(priceController.text) *
            double.parse(quantityController.text));
      }
      triggerController = false;
      totalController.text = totalTemp.toString();
      total = totalTemp;
      triggerController = true;
    }
  }

  handleQuantity() {
    calculateBudget();
    calculateTotal();
  }

  void _update(ctx) async {
    // row to insert
    Map<String, dynamic> row = {
      "id": id,
      "title": title,
      "todoType": todoType.index,
      "maxPrice": maxPrice,
      "quantity": quantity,
      "unit": unit,
      "budget": budget,
      "price": price,
      "total": total,
      "remarks": remarks,
      "entryDate": entryDate.toString().substring(0, 23),
      "todoStatus": todoStatus.index,
      "completionDate": completionDate.toString().substring(0, 23),
      "completionNote": completionNote
    };
    final updatedStatus = await widget.dbHelper.update(row);
    print('updated: $updatedStatus');
    Navigator.pushReplacementNamed(context, '/');
  }

  @override
  void initState() {
    maxPriceController.addListener(calculateBudget);
    quantityController.addListener(handleQuantity);
    budgetController.addListener(calculateMaxPrice);
    priceController.addListener(calculateTotal);
    totalController.addListener(calculatePrice);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (pageShifted) {
      final Map<String, dynamic> args =
          ModalRoute.of(context).settings.arguments;

      id = args['id'].toString();
      title = args['title'].toString();
      maxPrice = double.parse(args['maxPrice'].toString());
      quantity = double.parse(args['quantity'].toString());
      unit = args['unit'] != null ? args['unit'].toString() : "";
      budget = double.parse(args['budget'].toString());
      price = double.parse(args['price'].toString());
      total = double.parse(args['total'].toString());
      remarks = args['remarks'].toString();
      entryDate = DateTime.parse(args['entryDate'].toString());
      completionDate = DateTime.parse(args['completionDate'].toString());
      completionNote = args['completionNote'].toString();

      setState(() {
        todoType = TodoType.values[int.parse(args['todoType'].toString())];
        todoStatus =
            TodoStatus.values[int.parse(args['todoStatus'].toString())];
      });

      titleController.text = args['title'].toString();
      maxPriceController.text = args['maxPrice'].toString();
      quantityController.text = args['quantity'].toString();
      unitController.text = args['unit'] != null ? args['unit'].toString() : "";
      budgetController.text = args['budget'].toString();
      priceController.text = args['price'].toString();
      totalController.text = args['total'].toString();
      remarksController.text = args['remarks'].toString();
      completionNoteController.text = args['completionNote'].toString();

      pageShifted = false;
    }

    return Scaffold(
      appBar: AppBar(title: Text("Edit Todo"), actions: <Widget>[
        // action button
        IconButton(
          icon: Icon(Icons.home),
          onPressed: () {
            Navigator.pushReplacementNamed(context, '/');
          },
        ),
      ]),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width - 10,
        padding: EdgeInsets.all(5),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(labelText: 'Title'),
                  controller: titleController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter Title.';
                    } else {
                      title = value;
                      return null;
                    }
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 15,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  color: Colors.grey[300],
                  width: double.infinity,
                  child: new DropdownButton<TodoType>(
                    value: todoType,
                    isExpanded: true,
                    hint: Text('Please choose a type'),
                    items: TodoType.values.map((TodoType todoType) {
                      return DropdownMenuItem<TodoType>(
                          value: todoType,
                          child: Text(todoType
                              .toString()
                              .replaceAll("_", " ")
                              .split('.')
                              .last));
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        todoType = value;
                      });
                    },
                  ),
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Max Price'),
                  keyboardType: TextInputType.number,
                  controller: maxPriceController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter a valid Max Price.';
                    } else if (double.parse(value) < 0) {
                      return 'Please enter a valid Max Price.';
                    } else {
                      setState(() {
                        maxPrice = double.parse(value);
                      });
                      return null;
                    }
                  },
                  onFieldSubmitted: (value) {
                    if (value.isEmpty) {
                      setState(() {
                        maxPrice = 0;
                      });
                    } else {
                      setState(() {
                        maxPrice = double.parse(value);
                      });

                      print(maxPrice);
                    }
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Quantity'),
                  keyboardType: TextInputType.number,
                  controller: quantityController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter a valid Quantity.';
                    } else if (double.parse(value) < 0) {
                      return 'Please enter a valid Quantity.';
                    } else {
                      setState(() {
                        quantity = double.parse(value);
                      });
                      return null;
                    }
                  },
                  onFieldSubmitted: (value) {
                    if (value.isEmpty) {
                      setState(() {
                        quantity = 0;
                      });
                    } else {
                      setState(() {
                        quantity = double.parse(value);
                      });
                    }
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Unit'),
                  controller: unitController,
                  validator: (value) {
                    if (value.isEmpty) {
                      //return 'Please enter Unit.';
                      return null;
                    } else {
                      unit = value;
                      return null;
                    }
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 15,
                ),
                TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(labelText: 'Budget'),
                  controller: budgetController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter a valid Budget.';
                    } else if (double.parse(value) < 0) {
                      return 'Please enter a valid Budget.';
                    } else {
                      setState(() {
                        budget = double.parse(value);
                      });
                      return null;
                    }
                  },
                  onFieldSubmitted: (value) {
                    if (value.isEmpty) {
                      setState(() {
                        budget = 0;
                      });
                    } else {
                      setState(() {
                        budget = double.parse(value);
                      });
                    }
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Purchase Price'),
                  keyboardType: TextInputType.number,
                  controller: priceController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter a valid Purchase Price.';
                    } else if (double.parse(value) < 0) {
                      return 'Please enter a valid Purchase Price.';
                    } else {
                      setState(() {
                        price = double.parse(value);
                      });
                      return null;
                    }
                  },
                  onFieldSubmitted: (value) {
                    if (value.isEmpty) {
                      setState(() {
                        price = 0;
                      });
                    } else {
                      setState(() {
                        price = double.parse(value);
                      });

                      print(price);
                    }
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Total Price'),
                  keyboardType: TextInputType.number,
                  controller: totalController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter a valid Total Price.';
                    } else if (double.parse(value) < 0) {
                      return 'Please enter a valid Total Price.';
                    } else {
                      setState(() {
                        total = double.parse(value);
                      });
                      return null;
                    }
                  },
                  onFieldSubmitted: (value) {
                    if (value.isEmpty) {
                      setState(() {
                        total = 0;
                      });
                    } else {
                      setState(() {
                        total = double.parse(value);
                      });

                      print(total);
                    }
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Remarks'),
                  controller: remarksController,
                  validator: (value) {
                    remarks = value;
                    return null;
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 15,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  width: double.infinity,
                  color: Colors.grey[300],
                  child: new DropdownButton<TodoStatus>(
                    value: todoStatus,
                    isExpanded: true,
                    hint: Text('Please choose a Status'),
                    items: TodoStatus.values.map((TodoStatus todoStatus) {
                      return DropdownMenuItem<TodoStatus>(
                          value: todoStatus,
                          child: Text(todoStatus
                              .toString()
                              .replaceAll("_", " ")
                              .split('.')
                              .last));
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        todoStatus = value;
                      });
                    },
                  ),
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                RaisedButton(
                    textColor: Colors.white,
                    color: Theme.of(context).primaryColor,
                    child: Text('Choose a date'),
                    onPressed: () async {
                      DateTime tempDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime.now().subtract(Duration(days: 30)),
                        lastDate: DateTime.now().add(Duration(days: 30)),
                      );

                      setState(() {
                        entryDate = tempDate;
                      });
                    }),
                Text(
                    "Date:${new DateFormat.yMMMd().format(entryDate).toString()}"),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                RaisedButton(
                    textColor: Colors.white,
                    color: Theme.of(context).primaryColor,
                    child: Text('Choose a Completion date'),
                    onPressed: () async {
                      DateTime tempDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime.now().subtract(Duration(days: 30)),
                        lastDate: DateTime.now().add(Duration(days: 30)),
                      );

                      setState(() {
                        completionDate = tempDate;
                      });
                    }),
                Text(
                    "Date:${new DateFormat.yMMMd().format(completionDate).toString()}"),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Completion Note'),
                  controller: completionNoteController,
                  validator: (value) {
                    completionNote = value;
                    return null;
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: RaisedButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _update(context);
                      }
                    },
                    child: Text('Submit'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
