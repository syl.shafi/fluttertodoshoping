import 'package:flutter/material.dart';
import '../models/todo.dart';

class TodoStatusWidget extends StatelessWidget {
  
  final TodoStatus todoStatus;

  TodoStatusWidget({this.todoStatus});

  @override
  Widget build(BuildContext context) {
    if (TodoStatus.listed == todoStatus) {
      return CircleAvatar(
        child: Icon(
          Icons.shopping_cart,
          color: Colors.white,
        ),
        radius: 40,
        backgroundColor: Theme.of(context).accentColor,
      );
    }
    else if (TodoStatus.completed == todoStatus) {
      return CircleAvatar(
        child: Icon(
          Icons.check,
          color: Colors.white,
          size: 40.0,
        ),
        radius: 40,
        backgroundColor: Theme.of(context).accentColor,
      );
    }
    else if (TodoStatus.out_of_budget == todoStatus) {
      return CircleAvatar(
        child: Icon(
          Icons.attach_money,
          color: Colors.white,
          size: 40.0,
        ),
        radius: 40,
        backgroundColor: Colors.red,
      );
    }
    else if (TodoStatus.low_quality == todoStatus) {
      return CircleAvatar(
        child: Icon(
          Icons.sentiment_very_dissatisfied,
          color: Colors.white,
          size: 40.0,
        ),
        radius: 40,
        backgroundColor: Colors.red,
      );
    }
    else if (TodoStatus.seasonal_case == todoStatus) {
      return CircleAvatar(
        child: Icon(
          Icons.local_florist,
          color: Colors.white,
          size: 40.0,
        ),
        radius: 40,
        backgroundColor: Colors.red,
      );
    }
    else if (TodoStatus.expired == todoStatus) {
      return CircleAvatar(
        child: Icon(
          Icons.alarm,
          color: Colors.white,
          size: 40.0,
        ),
        radius: 40,
        backgroundColor: Colors.red,
      );
    }
    else{
      return CircleAvatar(
        child: Icon(
          Icons.remove_shopping_cart,
          color: Colors.white,
        ),
        radius: 40,
        backgroundColor: Colors.red,
      );
    }
  }
}
