import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../models/todo.dart';
import '../models/todo_manager.dart';

class CreateTodo extends StatefulWidget {
  final dbHelper = TodoManager.instance;

  @override
  CreateTodoState createState() {
    return new CreateTodoState();
  }
}

class CreateTodoState extends State<CreateTodo> {
  final _formKey = GlobalKey<FormState>();

  final quantityController = TextEditingController();
  final maxPriceController = TextEditingController();
  final budgetController = TextEditingController();

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  String id;
  String title;
  TodoType todoType = TodoType.kitchen;
  double maxPrice = 0;
  double quantity = 0;
  String unit;
  double budget = 0;
  double price = 0;
  double total = 0;
  String remarks;
  DateTime entryDate = DateTime.now();
  TodoStatus todoStatus = TodoStatus.listed;
  DateTime completionDate = DateTime.now();
  String completionNote;

  bool triggerController = true;

  calculatePrice() {
    if (triggerController) {
      double maxPriceTemp = 0.0;

      if (quantityController.text.isEmpty || budgetController.text.isEmpty) {
        maxPriceTemp = 0.0;
      } else if (!isNumeric(quantityController.text) ||
          !isNumeric(budgetController.text)) {
        maxPriceTemp = 0.0;
      } else if (double.parse(quantityController.text) == 0) {
        maxPriceTemp = 0.0;
      } else {
        maxPriceTemp = (double.parse(budgetController.text) /
            double.parse(quantityController.text));
      }
      triggerController = false;
      maxPriceController.text = maxPriceTemp.toString();
      maxPrice = maxPriceTemp;
      triggerController = true;
    }
  }

  calculateBudget() {
    if (triggerController) {
      double budgetTemp = 0.0;

      if (quantityController.text.isEmpty || maxPriceController.text.isEmpty) {
        budgetTemp = 0.0;
      } else if (!isNumeric(quantityController.text) ||
          !isNumeric(maxPriceController.text)) {
        budgetTemp = 0.0;
      } else {
        budgetTemp = (double.parse(maxPriceController.text) *
            double.parse(quantityController.text));
      }
      triggerController = false;
      budgetController.text = budgetTemp.toString();
      budget = budgetTemp;
      triggerController = true;
    }
  }

  void _insert(ctx) async {
    // row to insert
    Map<String, dynamic> row = {
      "title": title,
      "todoType": todoType.index,
      "maxPrice": maxPrice,
      "quantity": quantity,
      "unit": unit,
      "budget": budget,
      "price": 0,
      "total": 0,
      "remarks": "",
      "entryDate": entryDate.toString().substring(0, 23),
      "todoStatus": todoStatus.index,
      "completionDate": entryDate.toString().substring(0, 23),
      "completionNote": ""
    };
    final id = await widget.dbHelper.insert(row);
    print('inserted row id: $id');
    Navigator.pushReplacementNamed(context, '/');
  }

  @override
  void initState() {
    maxPriceController.addListener(calculateBudget);
    quantityController.addListener(calculateBudget);
    budgetController.addListener(calculatePrice);

    maxPriceController.text = "0";
    quantityController.text = "1";
    budgetController.text = "0";

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("New Todo"), actions: <Widget>[
        // action button
        IconButton(
          icon: Icon(Icons.home),
          onPressed: () {
            Navigator.pushReplacementNamed(context, '/');
          },
        ),
      ]),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width - 10,
        padding: EdgeInsets.all(5),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(labelText: 'Title'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter Title.';
                    } else {
                      title = value;
                      return null;
                    }
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 15,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  color: Colors.grey[300],
                  width: double.infinity,
                  child: new DropdownButton<TodoType>(
                    value: todoType,
                    isExpanded: true,
                    hint: Text('Please choose a type'),
                    items: TodoType.values.map((TodoType todoType) {
                      return DropdownMenuItem<TodoType>(
                          value: todoType,
                          child: Text(todoType
                              .toString()
                              .replaceAll("_", " ")
                              .split('.')
                              .last));
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        todoType = value;
                      });
                    },
                  ),
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Max Price'),
                  keyboardType: TextInputType.number,
                  controller: maxPriceController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter a valid Max Price.';
                    } else if (double.parse(value) < 0) {
                      return 'Please enter a valid Max Price.';
                    } else {
                      setState(() {
                        maxPrice = double.parse(value);
                      });
                      return null;
                    }
                  },
                  onFieldSubmitted: (value) {
                    if (value.isEmpty) {
                      setState(() {
                        maxPrice = 0;
                      });
                    } else {
                      setState(() {
                        maxPrice = double.parse(value);
                      });

                      print(maxPrice);
                    }
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Quantity'),
                  keyboardType: TextInputType.number,
                  controller: quantityController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter a valid Quantity.';
                    } else if (double.parse(value) < 0) {
                      return 'Please enter a valid Quantity.';
                    } else {
                      setState(() {
                        quantity = double.parse(value);
                      });
                      return null;
                    }
                  },
                  onFieldSubmitted: (value) {
                    if (value.isEmpty) {
                      setState(() {
                        quantity = 0;
                      });
                    } else {
                      setState(() {
                        quantity = double.parse(value);
                      });
                    }
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Unit'),
                  validator: (value) {
                    if (value.isEmpty) {
                      //return 'Please enter Unit.';
                      return null;
                    } else {
                      unit = value;
                      return null;
                    }
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 15,
                ),
                TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(labelText: 'Budget'),
                  controller: budgetController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter a valid Budget.';
                    } else if (double.parse(value) < 0) {
                      return 'Please enter a valid Budget.';
                    } else {
                      setState(() {
                        budget = double.parse(value);
                      });
                      return null;
                    }
                  },
                  onFieldSubmitted: (value) {
                    if (value.isEmpty) {
                      setState(() {
                        budget = 0;
                      });
                    } else {
                      setState(() {
                        budget = double.parse(value);
                      });
                    }
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Remarks'),
                  validator: (value) {
                    remarks = value;
                    return null;
                  },
                ),
                SizedBox(
                  width: double.infinity,
                  height: 15,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  width: double.infinity,
                  color: Colors.grey[300],
                  child: new DropdownButton<TodoStatus>(
                    value: todoStatus,
                    isExpanded: true,
                    hint: Text('Please choose a Status'),
                    items: TodoStatus.values.map((TodoStatus todoStatus) {
                      return DropdownMenuItem<TodoStatus>(
                          value: todoStatus,
                          child: Text(todoStatus
                              .toString()
                              .replaceAll("_", " ")
                              .split('.')
                              .last));
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        todoStatus = value;
                      });
                    },
                  ),
                ),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                RaisedButton(
                    textColor: Colors.white,
                    color: Theme.of(context).primaryColor,
                    child: Text('Choose a date'),
                    onPressed: () async {
                      DateTime tempDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime.now().subtract(Duration(days: 30)),
                        lastDate: DateTime.now().add(Duration(days: 30)),
                      );

                      setState(() {
                        entryDate = tempDate;
                      });
                    }),
                Text(
                    "Date:${new DateFormat.yMMMd().format(entryDate).toString()}"),
                SizedBox(
                  width: double.infinity,
                  height: 5,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: RaisedButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _insert(context);
                      }
                    },
                    child: Text('Submit'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
