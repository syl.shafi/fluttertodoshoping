import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class TodoManager {
  static final _databaseName = "todo_shoping.db";
  static final _databaseVersion = 1;

  static final table = 'Todo';

  // make this a singleton class
  TodoManager._privateConstructor();
  static final TodoManager instance = TodoManager._privateConstructor();

  // only have a single app-wide reference to the database
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    // lazily instantiate the db the first time it is accessed
    _database = await _initDatabase();
    return _database;
  }

  // this opens the database (and creates it if it doesn't exist)
  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  // SQL code to create the database table
  Future _onCreate(Database db, int version) async {
    await db.execute(
      "CREATE TABLE Todo(id INTEGER PRIMARY KEY, title TEXT, todoType INTEGER, maxPrice DOUBLE, quantity DOUBLE, unit TEXT, budget DOUBLE, price DOUBLE, total DOUBLE, remarks TEXT, entryDate DATETIME, todoStatus INTEGER, completionDate DATETIME, completionNote TEXT)",
    );

  }

  Future<int> insert(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(table, row);
  }

  Future<List<Map<String, dynamic>>> queryAllRows(String searchText) async {
    Database db = await instance.database;
    List<Map<String, dynamic>> rows = await db.query(table, where: ' title LIKE ?  ', whereArgs: ['%$searchText%']);
    return rows;
  }

  Future<int> update(Map<String, dynamic> row) async {
    Database db = await instance.database;
    int id = int.parse(row['id']);
    return await db.update(table, row, where: 'id = ?', whereArgs: [id]);
  }

  Future<int> delete(int id) async {
    Database db = await instance.database;
    return await db.delete(table, where: 'id = ?', whereArgs: [id]);
  }


}
