enum TodoStatus {
  listed,
  completed,
  not_found,
  out_of_budget,
  low_quality,
  expired,
  seasonal_case
}

enum TodoType {
   kitchen,
   wearing,
   health,
   education,
   ready_food,
   electronic,
   tool,
   fun_and_sports,
   financial,
}

class Todo {
  final String id;
  final String title;
  final TodoType todoType;
  final double maxPrice;
  final double quantity;
  final String unit;
  final double budget;
  final double price;
  final double total;
  final String remarks;
  final DateTime entryDate;
  final TodoStatus todoStatus;
  final DateTime completionDate;
  final String completionNote;

  Todo(
      {this.id,
      this.title,
      this.todoType,
      this.maxPrice,
      this.quantity,
      this.unit,
      this.budget,
      this.price,
      this.total,
      this.remarks,
      this.entryDate,
      this.todoStatus,
      this.completionDate,
      this.completionNote
      });

     
  
}
